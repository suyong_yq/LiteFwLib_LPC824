/* bsp_config.c */
#include "bsp_config.h"
//#include "chip_clock_8xx.h"
#include "chip_syscon_8xx.h"
#include "chip_swm_8xx.h"
#include "chip_uart_8xx.h"

void BSP_InitDebugUART(void)
{
    /* 在SWM中配置关联引脚 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM);
    /* USARTx_RX - P0_0
     * USARTx_TX - P0_4
     */
    /* 先关闭两个引脚的模拟信号复用功能
     * 若是没有固定的模拟信号复用，则可跳过这一步
     */
    Chip_SWM_DisableFixedPin(eSWM_FixedPinSel_ACMP_I1);
    Chip_SWM_DisableFixedPin(eSWM_FixedPinSel_ADC_Chn11);
    /* 分配USARTx信号到指定引脚上 */
    /* USART0. */
    Chip_SWM_MovablePinAssign(SWM_U0_TXD_O, 4);
    Chip_SWM_MovablePinAssign(SWM_U0_RXD_I, 0);
#if 0
    /* USART1. */
    Chip_SWM_MovablePinAssign(SWM_U1_TXD_O, 4);
    Chip_SWM_MovablePinAssign(SWM_U1_RXD_I, 0);
    /* USART2. */
    Chip_SWM_MovablePinAssign(SWM_U2_TXD_O, 4);
    Chip_SWM_MovablePinAssign(SWM_U2_RXD_I, 0);
#endif
    /* 关闭SWM时钟以省电 */
    Chip_SYSCON_DisablePeriphClock(SYSCON_AHBPeriphClk_SWM);


    //Chip_SYSCON_ConfigUARTCommonClockDivier(30, 21, 0xFF);
    //Chip_UART_SetBaudrateDivider(BSP_DEBUG_UART_BASEADDR, 115200/baudrate - 1U); /* 输出波特率 */

    Chip_SYSCON_SetUARTCommonBaudrate(BSP_CLK_MAIN_CLK_HZ, BSP_UART_COMMON_BAUDRATE);
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_UART0); /* 启用UART0模块访问时钟 */

    /* 配置UART传输数据格式 */
    Chip_UART_ConfigData(BSP_DEBUG_UART_BASEADDR,
            UART_CFG_DATALEN_8   /* 8位有效数据 */
          | UART_CFG_PARITY_NONE /* 无校验位 */
          | UART_CFG_STOPLEN_1   /* 1位停止位 */
          );
    //Chip_UART_SetBaud(BSP_DEBUG_UART_BASEADDR, 115200); /* 设定通信波特率 */
    Chip_UART_Enable(BSP_DEBUG_UART_BASEADDR); /* 启用收发器逻辑电路 */
    Chip_UART_TXEnable(BSP_DEBUG_UART_BASEADDR); /* 启用发送引擎 */
}

/* EOF. */

