/* bsp_config.h */
#ifndef __BSP_CONFIG_H__
#define __BSP_CONFIG_H__

#include <stdint.h>
#include "chip.h"

/* Clocks. */
#define BSP_CLK_MAIN_CLK_HZ   60000000UL /* 60MHz. */
#define BSP_CLK_SYSTEM_CLK_HZ 30000000UL /* 30MHz. */

/* UARTs. */
#define BSP_UART_COMMON_BAUDRATE 115200UL
#define BSP_DEBUG_UART_BASEADDR LPC_USART0

/* GPIO - LED. */
#define BSP_GPIO_LEDx_BASEADDR LPC_GPIO
#define BSP_GPIO_LEDx_PORT_ID  0U
/* LPC824-Lite开发板上的丝印比原理图上的编号大1，从1开始编号 */
#define BSP_GPIO_LED0_PIN_ID   7U
#define BSP_GPIO_LED1_PIN_ID   13U
#define BSP_GPIO_LED2_PIN_ID   16U
#define BSP_GPIO_LED3_PIN_ID   17U
#define BSP_GPIO_LED4_PIN_ID   19U
#define BSP_GPIO_LED5_PIN_ID   27U
#define BSP_GPIO_LED6_PIN_ID   28U
#define BSP_GPIO_LED7_PIN_ID   18U

/* GPIO - Key. */
#define BSP_GPIO_KEYx_BASEADDR     LPC_GPIO
#define BSP_GPIO_KEYx_PORT_ID      0U
/* 此处罗列的Key名称与LPC824-Lite开发板的丝印一致 */
#define BSP_GPIO_KEY_USER_PIN_ID   1U
#define BSP_GPIO_KEY_WAKEUP_PIN_ID 4U
#define BSP_GPIO_KEY_ISP_PIN_ID    12U

/* ADC - Variable Resistor. */
#define BSP_ADC_VAR_RES_CHN_ID   3U
#define BSP_GPIO_VAR_RES_PIN_ID  23U

#define BSP_SPI_EXT_FLASH_BASEADDR  LPC_SPI0

void BSP_InitDebugUART(void);

#endif /* __BSP_CONFIG_H__ */

