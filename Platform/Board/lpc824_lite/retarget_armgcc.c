/*
 * @brief   IO redirection support
 *
 * This file adds re-direction support to the library for various
 * projects. It can be configured in one of 3 ways - no redirection,
 * redirection via a UART, or redirection via semihosting. If DEBUG
 * is not defined, all printf statements will do nothing with the
 * output being throw away. If DEBUG is defined, then the choice of
 * output is selected by the DEBUG_SEMIHOSTING define. If the
 * DEBUG_SEMIHOSTING is not defined, then output is redirected via
 * the UART. If DEBUG_SEMIHOSTING is defined, then output will be
 * attempted to be redirected via semihosting. If the UART method
 * is used, then the Board_UARTPutChar and Board_UARTGetChar
 * functions must be defined to be used by this driver and the UART
 * must already be initialized to the correct settings.
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board.h"

#if defined( __GNUC__ )
/* Include stdio.h to pull in __REDLIB_INTERFACE_VERSION__ */
#include <stdio.h>

#if defined(__NEWLIB__)
    #define WRITEFUNC _write
    #define READFUNC _read
#else
#if (__REDLIB_INTERFACE_VERSION__ >= 20000)
/* We are using new Redlib_v2 semihosting interface */
    #define WRITEFUNC __sys_write
    #define READFUNC __sys_readc
#else
/* We are using original Redlib semihosting interface */
    #define WRITEFUNC __write
    #define READFUNC __readc
#endif
#endif /* __NEWLIB__ */

#if defined(DEBUG_ENABLE)
#if defined(DEBUG_SEMIHOSTING)
/* Do nothing, semihosting is enabled by default in LPCXpresso */
#endif /* defined(DEBUG_SEMIHOSTING) */
#endif /* defined(DEBUG_ENABLE) */

#if !defined(DEBUG_SEMIHOSTING)
int WRITEFUNC(int iFileHandle, char *pcBuffer, int iLength)
{
#if defined(DEBUG_ENABLE)
    unsigned int i;
    for (i = 0; i < iLength; i++) {
        Board_UARTPutChar(pcBuffer[i]);
    }
#endif

    return iLength;
}

#if !defined(__NEWLIB__)
/* Called by bottom level of scanf routine within RedLib C library to read
   a character. With the default semihosting stub, this would read the character
   from the debugger console window (which acts as stdin). But this version reads
   the character from the LPC1768/RDB1768 UART. */
int READFUNC(void)
{
#if defined(DEBUG_ENABLE)
    int c = Board_UARTGetChar();
    return c;

#else
    return (int) -1;
#endif
}
#else
/*
 * **WARNING**: THIS FUNCTION IS NON-BLOCKING
 * Not just STDIN all inputs handled via UART
 * Read function for newlib is added as a non-blocking function
 * the application should check for the size to identify the number
 * of bytes received */
int READFUNC(int iFileHandle, char *pcBuffer, int iLength)
{
    int idx;
    for (idx = 0; idx < iLength; idx++) {
        int c = Board_UARTGetChar();
        if (c == EOF) break;
        pcBuffer[idx] = c;
    }
    return idx;
}
#endif

#endif /* !defined(DEBUG_SEMIHOSTING) */
#endif /* defined ( __GNUC__ ) */
