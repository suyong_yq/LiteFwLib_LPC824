/* retarget_ccarm.c */
#include "bsp_config.h"
#include <stdio.h>
#include "chip_uart_8xx.h"


struct __FILE {
	int handle;
};

FILE __stdout;
FILE __stdin;
FILE __stderr;

void *_sys_open(const char *name, int openmode)
{
	return 0;
}

int fputc(int c, FILE *f)
{
    Chip_UART_SendBlocking(BSP_DEBUG_UART_BASEADDR, &c, 1);
    return c;
}

int fgetc(FILE *f)
{
    uint8_t ch;
    
    while (Chip_UART_Read(BSP_DEBUG_UART_BASEADDR, &ch, 1) != 1)
	{}
    return ch;
}

int ferror(FILE *f)
{
	return EOF;
}

void _sys_exit(int return_code)
{
    while (1)
    {
	    __WFI();
	}
}
/* EOF. */

