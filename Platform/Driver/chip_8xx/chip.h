/*
 * @brief LPC8xx basic chip inclusion file
 *
 * @note
 * 替代原LPCOpen的chip.h文件
 */

#ifndef __CHIP_H_
#define __CHIP_H_

#define CORE_M0PLUS
#define CHIP_LPC82X

#include <stdint.h>
#include <stdbool.h>

#include "LPC82x.h"
#include "core_cm0plus.h"

#endif /* __CHIP_H_ */

