/*
 * @brief LPC8xx ADC driver
 * @author suyong_yq@126.com
 *
 * @note
 */
#include "chip_adc_8xx.h"
#include <string.h>

void Chip_ADC_SetConvClkDiv(LPC_ADC_T *base, uint8_t divVal)
{
    base->CTRL = (base->CTRL & ~ADC_CTRL_CLKDIV_MASK) | ADC_CTRL_CLKDIV(divVal);
}

void Chip_ADC_EnableAutoPowerSwitch(LPC_ADC_T *base, bool enable)
{
    if (enable)
    {
        base->CTRL |= ADC_CTRL_LPWRMODE_MASK;
    }
    else
    {
        base->CTRL &= ~ADC_CTRL_LPWRMODE_MASK;
    }
}

void Chip_ADC_DoHwSelfCalibratioin(LPC_ADC_T *base, uint8_t divVal)
{
    base->CTRL = ADC_CTRL_CLKDIV(divVal) | ADC_CTRL_CALMODE_MASK;
    
    /* 等待校准完成，硬件自动清ADC_CTRL[CALMODE]. */
    while(0U != (ADC_CTRL_CALMODE_MASK & base->CTRL))
    {}
}

void Chip_ADC_EnableConvSeq(LPC_ADC_T *base, uint32_t seqIdx, bool enable)
{
    if (enable)
    {
        base->SEQ_CTRL[seqIdx] |= ADC_SEQ_CTRL_SEQ_ENA_MASK;
    }
    else
    {
        base->SEQ_CTRL[seqIdx] &= ~ADC_SEQ_CTRL_SEQ_ENA_MASK;
    }
}

void Chip_ADC_DoConvSeqSoftTrigger(LPC_ADC_T *base, uint32_t seqIdx)
{
    base->SEQ_CTRL[seqIdx] |= ADC_SEQ_CTRL_START_MASK;
}

void Chip_ADC_EnableConvSeqBurst(LPC_ADC_T *base, uint32_t seqIdx, bool enable)
{
    if (enable)
    {
        base->SEQ_CTRL[seqIdx] |= ADC_SEQ_CTRL_BURST_MASK;
    }
    else
    {
        base->SEQ_CTRL[seqIdx] &= ~ADC_SEQ_CTRL_BURST_MASK;
    }
}

void Chip_ADC_ConfigConvSeq(LPC_ADC_T *base, uint32_t seqIdx, const ADC_SeqConfig_T *config)
{
    uint32_t tmp32 = 0U;

    tmp32 = ADC_SEQ_CTRL_CHANNELS(config->ChannelMaskValue)
          | ADC_SEQ_CTRL_TRIGGER(config->Trigger)
          | ADC_SEQ_CTRL_TRIGPOL(config->TriggerInputPolarity)
          | ADC_SEQ_CTRL_LOWPRIO(config->Priority)
          | ADC_SEQ_CTRL_MODE(config->InterruptMode);

    if (config->EnableHwTriggerSyncBypass)
    {
        tmp32 |= ADC_SEQ_CTRL_SYNCBYPASS_MASK;
    }
    if (config->EnableSingleStepMode)
    {
        tmp32 |= ADC_SEQ_CTRL_SINGLESTEP_MASK;
    }

    base->SEQ_CTRL[seqIdx] = tmp32;
}

void Chip_ADC_GetConvSeqGlobalResult(LPC_ADC_T *base, uint32_t seqIdx, ADC_ResultInfo_T *info)
{
    uint32_t tmp32 = base->SEQ_GDAT[seqIdx];
    
    //*info = {0};
    memset(info, 0U, sizeof(ADC_ResultInfo_T)); /* Reset the info structure. */
    info->Result = (tmp32 & ADC_SEQ_GDAT_RESULT_MASK) >> ADC_SEQ_GDAT_RESULT_SHIFT;
    info->CompareMode = (ADC_ConvResultCompareMode_T)((tmp32 & ADC_SEQ_GDAT_THCMPRANGE_MASK) >> ADC_SEQ_GDAT_THCMPRANGE_SHIFT);
    info->CrossingMode = (ADC_ConvResultCrossingMode_T)((tmp32 & ADC_SEQ_GDAT_THCMPCROSS_MASK) >> ADC_SEQ_GDAT_THCMPCROSS_SHIFT);
    info->ChannelIdx = (tmp32 & ADC_SEQ_GDAT_CHN_MASK) >> ADC_SEQ_GDAT_CHN_SHIFT;
    if (0U != (ADC_SEQ_GDAT_OVERRUN_MASK & tmp32))
    {
        info->OnOverrun = true;
    }
    if (0U != (ADC_SEQ_GDAT_DATAVALID_SHIFT & tmp32))
    {
        info->OnDataAvailable = true;
    }
}

void Chip_ADC_GetConvChannelResult(LPC_ADC_T *base, uint32_t chnIdx, ADC_ResultInfo_T *info)
{
    uint32_t tmp32 = base->DAT[chnIdx];

    info->Result = (tmp32 & ADC_SEQ_GDAT_RESULT_MASK) >> ADC_SEQ_GDAT_RESULT_SHIFT;
    info->CompareMode = (ADC_ConvResultCompareMode_T)((tmp32 & ADC_SEQ_GDAT_THCMPRANGE_MASK) >> ADC_SEQ_GDAT_THCMPRANGE_SHIFT);
    info->CrossingMode = (ADC_ConvResultCrossingMode_T)((tmp32 & ADC_SEQ_GDAT_THCMPCROSS_MASK) >> ADC_SEQ_GDAT_THCMPCROSS_SHIFT);
    info->ChannelIdx = (tmp32 & ADC_SEQ_GDAT_CHN_MASK) >> ADC_SEQ_GDAT_CHN_SHIFT;
    if (0U != (ADC_SEQ_GDAT_OVERRUN_MASK & tmp32))
    {
        info->OnOverrun = true;
    }
    if (0U != (ADC_SEQ_GDAT_DATAVALID_SHIFT & tmp32))
    {
        info->OnDataAvailable = true;
    }
}

void Chip_ADC_SetHwCompareThreshold(LPC_ADC_T *base, uint32_t groupIdx, uint32_t low, uint32_t high)
{
    base->THR_LOW[groupIdx]  = low;
    base->THR_HIGH[groupIdx] = high;
}

void Chip_ADC_EnableHwCompareChannel(LPC_ADC_T *base, uint32_t groupIdx, uint32_t chnMask)
{
    if (groupIdx == 0U) /* ADC_THR0_LOW & ADC_THR0_HIGH */
    {
        base->CHAN_THRSEL &= ~chnMask;
    }
    else if (groupIdx == 1U) /* ADC_THR1_LOW & ADC_THR1_HIGH */
    {
        base->CHAN_THRSEL |= chnMask;
    }
}

void Chip_ADC_EnableInterrupt(LPC_ADC_T *base, uint32_t intMask)
{
    base->INTEN |= intMask;
}

void Chip_ADC_DisableInterrupt(LPC_ADC_T *base, uint32_t intMask)
{
    base->INTEN &= ~intMask;
}

uint32_t Chip_ADC_GetStatusFlags(LPC_ADC_T *base)
{
    return base->FLAGS;
}

void Chip_ADC_ClearStatusFlags(LPC_ADC_T *base, uint32_t flagMask)
{
    base->FLAGS = flagMask;
}

/* EOF. */

