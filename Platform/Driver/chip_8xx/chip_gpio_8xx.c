/*
 * @brief LPC8xx GPIO driver
 * @author suyong_yq@126.com
 *
 * @note
 * 基于LPCOpen中gpio_8xx驱动程序改写
 */
#include "chip_gpio_8xx.h"

/* 配置引脚方向 */
void     Chip_GPIO_SetPortDir(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->DIR[portId] = pinMask;
}

void     Chip_GPIO_SetPortDirOutput(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->DIRSET[portId] = pinMask;
}

void     Chip_GPIO_SetPortDirInput(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->DIRCLR[portId] = pinMask;
}

void     Chip_GPIO_TogglePortDir(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->DIRNOT[portId] = pinMask;
}

/* 访问引脚状态 */
void     Chip_GPIO_WritePin(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId, uint8_t pinVal)
{
    pGPIO->B[portId][pinId] = pinVal;
}

uint8_t  Chip_GPIO_ReadPin(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId)
{
    return pGPIO->B[portId][pinId];
}

void     Chip_GPIO_WritePinWord(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId, uint32_t pinVal)
{
    pGPIO->W[portId][pinId] = pinVal;
}

uint32_t Chip_GPIO_ReadPinWord(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId)
{
    return pGPIO->W[portId][pinId];
}

void     Chip_GPIO_WritePort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t portVal)
{
    pGPIO->PIN[portId] = portVal;
}

uint32_t Chip_GPIO_ReadPort(LPC_GPIO_T *pGPIO, uint32_t portId)
{
    return pGPIO->PIN[portId];
}

void     Chip_GPIO_SetPort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->SET[portId] = pinMask;
}

void     Chip_GPIO_ClearPort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->CLR[portId] = pinMask;
}

void     Chip_GPIO_TogglePort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->NOT[portId] = pinMask;
}

/* 有保护的端口访问 */
void     Chip_GPIO_SetPortMask(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->MASK[portId] = pinMask;
}

void     Chip_GPIO_WritePortWithMask(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask)
{
    pGPIO->MPIN[portId] = pinMask;
}

uint32_t Chip_GPIO_ReadPortWithMask(LPC_GPIO_T *pGPIO, uint32_t portId)
{
    return pGPIO->MPIN[portId];
}

/* EOF. */

