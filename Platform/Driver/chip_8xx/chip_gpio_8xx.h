/*
 * @file chip_gpio.h
 * @author suyong_yq@126.com
 *
 * @brief LPC8xx GPIO driver
 * @note
 * 基于LPCOpen中gpio_8xx驱动程序改写
 */

#ifndef __GPIO_8XX_H_
#define __GPIO_8XX_H_

#include "chip.h"

#ifdef __cplusplus
extern "C" {
#endif

/* 配置引脚方向 */
void     Chip_GPIO_SetPortDir(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_SetPortDirOutput(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_SetPortDirInput(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_TogglePortDir(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);

/* 访问引脚状态 */
void     Chip_GPIO_WritePin(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId, uint8_t pinVal);
uint8_t  Chip_GPIO_ReadPin(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId);
void     Chip_GPIO_WritePinWord(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId, uint32_t pinVal);
uint32_t Chip_GPIO_ReadPinWord(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinId);

void     Chip_GPIO_WritePort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t portVal);
uint32_t Chip_GPIO_ReadPort(LPC_GPIO_T *pGPIO, uint32_t portId);
void     Chip_GPIO_SetPort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_ClearPort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_TogglePort(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);

/* 有保护的端口访问 */
void     Chip_GPIO_SetPortMask(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
void     Chip_GPIO_WritePortWithMask(LPC_GPIO_T *pGPIO, uint32_t portId, uint32_t pinMask);
uint32_t Chip_GPIO_ReadPortWithMask(LPC_GPIO_T *pGPIO, uint32_t portId);

#ifdef __cplusplus
}
#endif

#endif /* __GPIO_8XX_H_ */
