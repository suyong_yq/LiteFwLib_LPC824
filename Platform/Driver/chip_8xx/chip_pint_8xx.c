/*
 * @brief LPC8xx PINT driver
 * @author suyong_yq@126.com
 *
 * @note
 */
#include "chip_pint_8xx.h"

/* 启用中断检测 */
void Chip_PINT_SetPinInterruptMode(LPC_PINT_T *base, uint32_t pintIdx, PINT_TriggerMode_T mode)
{
    switch (mode)
    {
        case ePINT_TriggerByRisingEdge:
            base->ISEL &= ~(1U << pintIdx); /* 边沿触发 */
            base->SIENR = (1U << pintIdx);
            base->CIENF = (1U << pintIdx);
            break;
        case ePINT_TriggerByFallingEdge:
            base->ISEL &= ~(1U << pintIdx); /* 边沿触发 */
            base->CIENR = (1U << pintIdx);
            base->SIENF = (1U << pintIdx);
            break;
        case ePINT_TriggerByEitherEdge:
            base->ISEL &= ~(1U << pintIdx); /* 边沿触发 */
            base->SIENR = (1U << pintIdx);
            base->SIENF = (1U << pintIdx);
            break;
        case ePINT_TriggerByLowLevel:
            base->ISEL |= (1U << pintIdx); /* 电平触发 */
            base->CIENF = (1U << pintIdx);
            break;
        case ePINT_TriggerByHighLevel:
            base->ISEL |= (1U << pintIdx); /* 电平触发 */
            base->SIENF = (1U << pintIdx);
            break;
        default: /* ePINT_TriggerDisabled */
            base->ISEL &= ~(1U << pintIdx); /* 边沿触发，但不开任何边沿中断 */
            base->CIENR = (1U << pintIdx);
            base->CIENF = (1U << pintIdx);
            break;
    }
}

PINT_TriggerMode_T Chip_PINT_GetPinInterruptStatus(LPC_PINT_T *base, uint32_t pintIdx)
{
    uint32_t ret = (uint32_t)ePINT_TriggerDisabled;
    
    if ( 0U == (base->IST & (1U << pintIdx)) ) /* 没有中断触发产生 */
    {
        return (PINT_TriggerMode_T)ret;
    }
    
    if ( 0U == (base->ISEL & (1U << pintIdx)) ) /* 边沿触发 */
    {
        /* 上升沿 */
        if (   (0U != (base->IENR & (1U << pintIdx)))
            && (0U != (base->RISE & (1U << pintIdx))) )
        {
            ret |= (uint32_t)ePINT_TriggerByRisingEdge;
        }
        
        /* 下降沿 */
        if (   (0U != (base->IENF & (1U << pintIdx)))
            && (0U != (base->FALL & (1U << pintIdx))) )
        {
            ret |= (uint32_t)ePINT_TriggerByFallingEdge;
        }
    }
    else /* 电平触发 */
    {
        if (0U != (base->IENF & (1U << pintIdx)) )
        {
            ret = (uint32_t)ePINT_TriggerByHighLevel;
        }
        else
        {
            ret = (uint32_t)ePINT_TriggerByLowLevel;
        }
    }

    return (PINT_TriggerMode_T)ret;
}

/* 图省事，一次性全清干净 */
void Chip_PINT_ClearPinInterruptStatus(LPC_PINT_T *base, uint32_t pintIdx)
{
    /* 先清总标志位 */
    base->IST = (1U << pintIdx);
    
    /* 清边沿标志位 */
    base->RISE = (1U << pintIdx);
    base->FALL = (1U << pintIdx);
}

/* EOF. */

