/*
 * @file chip_pint.h
 * @author suyong_yq@126.com
 *
 * @brief LPC8xx PIN Interrupt driver
 * @note
 */

#ifndef __PINT_8XX_H_
#define __PINT_8XX_H_

#include "chip.h"

typedef enum
{
    ePINT_TriggerByRisingEdge  = (1U << 0),                  /*!< 上升沿触发 */
    ePINT_TriggerByFallingEdge = (1U << 1),                  /*!< 下降沿触发 */
    ePINT_TriggerByEitherEdge  = ( (1U << 0) | (1U << 1) ),  /*!< 任一边沿均可触发，不能当做状态标识使用 */
    ePINT_TriggerByLowLevel    = (1U << 2),
    ePINT_TriggerByHighLevel   = (1U << 3),
    ePINT_TriggerDisabled      = 0U,                         /*!< 关闭当前的触发配置 */
} PINT_TriggerMode_T;

#ifdef __cplusplus
extern "C" {
#endif

/* 配置引脚触发中断 */
void Chip_PINT_SetPinInterruptMode(LPC_PINT_T *base, uint32_t pintIdx, PINT_TriggerMode_T mode);
PINT_TriggerMode_T Chip_PINT_GetPinInterruptStatus(LPC_PINT_T *base, uint32_t pintIdx);
void Chip_PINT_ClearPinInterruptStatus(LPC_PINT_T *base, uint32_t pintIdx);

#ifdef __cplusplus
}
#endif

#endif /* __PINT_8XX_H_ */
