/* main.c */
#include "app_inc.h"

#define APP_ADC_VAR_ADC_CHN_IDX  3U /* 可调电阻连接ADC的输入通道 */

/* 用于保存转换结果 */
ADC_ResultInfo_T gAppAdcResultInfoStruct;
volatile ADC_ResultInfo_T *gAppAdcResultInfoPtr = &gAppAdcResultInfoStruct;

int main(void)
{
    ADC_SeqConfig_T AdcSeqConfigStruct;

    BSP_InitDebugUART(); /* 初始化调试串口 */

    printf("ADC_SingleChannelLoop.\r\n");
    printf("Initializing ...");
    
    /* 为ADC输入通道分配引脚 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM);
    Chip_SWM_EnableFixedPin(eSWM_FixedPinSel_ADC_Chn3); /* ADC_Ch3对应PIO0_23,在板子上连接电位器 */
    Chip_SYSCON_DisablePeriphClock(SYSCON_AHBPeriphClk_SWM);

    /* 为ADC转换器上电 */
    Chip_SYSCON_PowerUp(eSYSCON_PeriphPowerSel_ADC);
	
    /* 启用ADC寄存器访问接口时钟及模块时钟 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_ADC);
    
    /* 复位ADC寄存器 */
    Chip_SYSCON_PeriphReset(eSYSCON_PeriphReset_ADC);
    
    /* 初始化ADC转换器，AHB系统时钟是30MHz */
    Chip_ADC_DoHwSelfCalibratioin(LPC_ADC, 60U); /* 硬件自动校准 */

    Chip_ADC_SetConvClkDiv(LPC_ADC, 30U); /* 设定ADC转换器时钟从Bus Clock的分频 */
    //Chip_ADC_DisableInterrupt(LPC_ADC, eADC_InterruptAll); /* 确保所有中断都是关闭的 */
    
    Chip_ADC_EnableConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, false); /* 配置转换队列之前先停用 */

    AdcSeqConfigStruct.ChannelMaskValue = (1U << APP_ADC_VAR_ADC_CHN_IDX); /* ADC_Chn3. */
    AdcSeqConfigStruct.Trigger = eADC_TriggerBySoftTriggerOnly; /* 软件触发 */
    AdcSeqConfigStruct.TriggerInputPolarity = eADC_TriggerInputPolOnNegativeEdge; /* Dummy，只在硬件触发有效 */
    AdcSeqConfigStruct.EnableHwTriggerSyncBypass = false;
    AdcSeqConfigStruct.EnableSingleStepMode = false; /* 关闭单步触发模式，一次触发转换整个队列 */
    AdcSeqConfigStruct.Priority = eADC_ConvSeqPriorityHigh; /* 高优先级，总是优先转换 */
    AdcSeqConfigStruct.InterruptMode = eADC_ConvSeqInterruptOnEndOfSequence;/* 转换队列完成之后产生中断 */
    Chip_ADC_ConfigConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, &AdcSeqConfigStruct);

    Chip_ADC_EnableConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, true); /* 配置转换队列之后启用 */

    /* 启用ADC Seq A转换完成中断 */
    Chip_ADC_EnableInterrupt(LPC_ADC, eADC_InterruptOnSeqA);
    NVIC_EnableIRQ(ADC_SEQA_IRQn);

    printf("Done.\r\n");
    
    while (1)
    {
        getchar(); /* 终端按键触发转换 */
        
        gAppAdcResultInfoPtr->OnDataAvailable = false;
        Chip_ADC_DoConvSeqSoftTrigger(LPC_ADC, ADC_CONV_SEQ_IDX_A);
        
        /* 等待队列转换完成 */
        while ( !(gAppAdcResultInfoPtr->OnDataAvailable) )
        {
        }
        
        /* 输出显示转换结果 */
        printf("AdcConvValue: %d.\r\n", gAppAdcResultInfoPtr->Result);    
    }
}

/* ADC转换队列SeqA完成的中断服务程序 */
void ADC_SEQA_IRQHandler(void)
{
    if (eADC_StatusFlagOfInterruptOnConvSeqA 
        == (eADC_StatusFlagOfInterruptOnConvSeqA & Chip_ADC_GetStatusFlags(LPC_ADC)))
    {
        /* 清标志位 */
        Chip_ADC_ClearStatusFlags(LPC_ADC, eADC_StatusFlagOfInterruptOnConvSeqA);

        /* 从转换结果寄存器中获取数据 */
        Chip_ADC_GetConvChannelResult(LPC_ADC, APP_ADC_VAR_ADC_CHN_IDX, (ADC_ResultInfo_T *)gAppAdcResultInfoPtr);
        
        /* 当使用转换队列中断时，转换通道自己的结果寄存器的数据有效标志位不会自动置位，
         * 只有转换队列下属的结果寄存器才会自动置数据有效标志位。
         * 若是使用Chip_ADC_GetConvSeqGlobalResult()函数就可以自动填充数据有效标志位
         * 此处由软件将OnDataAvailable置位，作为向主循环传参的信号量。
         */
        gAppAdcResultInfoPtr->OnDataAvailable = true;
    }
}

/* EOF. */

