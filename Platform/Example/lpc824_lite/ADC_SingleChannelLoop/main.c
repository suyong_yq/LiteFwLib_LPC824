/* main.c */
#include "app_inc.h"

#define APP_ADC_VAR_ADC_CHN_IDX  3U /* 可调电阻连接ADC的输入通道 */

int main(void)
{
    ADC_SeqConfig_T AdcSeqConfigStruct;
    ADC_ResultInfo_T AdcResultInfoStruct;

    BSP_InitDebugUART(); /* 初始化调试串口 */

    printf("ADC_SingleChannelLoop.\r\n");
    printf("Initializing ...");
    
    /* 为ADC输入通道分配引脚 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM);
    Chip_SWM_EnableFixedPin(eSWM_FixedPinSel_ADC_Chn3); /* ADC_Ch3对应PIO0_23,在板子上连接电位器 */
    Chip_SYSCON_DisablePeriphClock(SYSCON_AHBPeriphClk_SWM);

    /* 为ADC转换器上电 */
    Chip_SYSCON_PowerUp(eSYSCON_PeriphPowerSel_ADC);
	
    /* 启用ADC寄存器访问接口时钟及模块时钟 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_ADC);
    
    /* 复位ADC寄存器 */
    Chip_SYSCON_PeriphReset(eSYSCON_PeriphReset_ADC);
    
    /* 初始化ADC转换器，AHB系统时钟是30MHz */
    Chip_ADC_DoHwSelfCalibratioin(LPC_ADC, 60U); /* 硬件自动校准 */

    Chip_ADC_SetConvClkDiv(LPC_ADC, 30U); /* 设定ADC转换器时钟从Bus Clock的分频 */
    //Chip_ADC_DisableInterrupt(LPC_ADC, eADC_InterruptAll); /* 确保所有中断都是关闭的 */
    
    Chip_ADC_EnableConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, false); /* 配置转换队列之前先停用 */

    AdcSeqConfigStruct.ChannelMaskValue = (1U << APP_ADC_VAR_ADC_CHN_IDX); /* ADC_Chn3. */
    AdcSeqConfigStruct.Trigger = eADC_TriggerBySoftTriggerOnly; /* 软件触发 */
    AdcSeqConfigStruct.TriggerInputPolarity = eADC_TriggerInputPolOnNegativeEdge; /* Dummy，只在硬件触发有效 */
    AdcSeqConfigStruct.EnableHwTriggerSyncBypass = false;
    AdcSeqConfigStruct.EnableSingleStepMode = false; /* 关闭单步触发模式，一次触发转换整个队列 */
    AdcSeqConfigStruct.Priority = eADC_ConvSeqPriorityHigh; /* 高优先级，总是优先转换 */
    AdcSeqConfigStruct.InterruptMode = eADC_ConvSeqInterruptOnEndOfSequence;/* 转换队列完成之后产生中断 */
    Chip_ADC_ConfigConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, &AdcSeqConfigStruct);

    Chip_ADC_EnableConvSeq(LPC_ADC, ADC_CONV_SEQ_IDX_A, true); /* 配置转换队列之后启用 */

    printf("Done.\r\n");
    
    while (1)
    {
        getchar(); /* 终端按键触发转换 */
        
        Chip_ADC_DoConvSeqSoftTrigger(LPC_ADC, ADC_CONV_SEQ_IDX_A);
        
        /* 等待队列转换完成 */
        do
        {
            Chip_ADC_GetConvChannelResult(LPC_ADC, APP_ADC_VAR_ADC_CHN_IDX, &AdcResultInfoStruct);
        } while (!AdcResultInfoStruct.OnDataAvailable);
        
        /* 读取转换结果并输出显示 */
        printf("AdcConvValue: %d.\r\n", AdcResultInfoStruct.Result);    
    }
}
/* EOF. */

