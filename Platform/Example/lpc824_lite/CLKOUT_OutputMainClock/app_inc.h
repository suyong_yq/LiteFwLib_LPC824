/* app_inc.h */

#ifndef __APP_INC_H__
#define __APP_INC_H__

#include <stdint.h>
#include <stdbool.h>

#include "bsp_config.h"

#include "chip_swm_8xx.h"
#include "chip_syscon_8xx.h"
#include "chip_iocon_8xx.h"

#endif /* __APP_INC_H__ */

