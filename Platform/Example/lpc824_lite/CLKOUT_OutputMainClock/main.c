/* main.c */
#include "app_inc.h"

void App_ClkoutOutputMainClock(void);

/*
 * 应用程序入口.
 */
int main(void)
{
    /* 配置CLKOUT输出Main Clock. */
    App_ClkoutOutputMainClock();

    while (1)
    {
        __NOP();
    }
}

/* 设定CLKOUT在PIO0_6引脚输出主时钟信号. */
void App_ClkoutOutputMainClock(void)
{
    /* 在SWM中配置关联引脚. */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM); /* 使用SWM之前启用接口访问时钟. */
    Chip_SWM_DisableFixedPin(SWM_FIXED_ADC1); /* 关闭PIO0_6引脚的模拟信号功能. */
    Chip_SWM_MovablePinAssign(SWM_CLKOUT_O, 6U); /* 将CLKOUT输出信号绑定到PIO0_6引脚上. */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM); /* 关闭SWM时钟以省电. */

    /* 配置CLKOUT输出.
     * 设定CLKOUT输出Main Clock, 分频值为1. */
    Chip_SYSCON_SetCLKOUTSource(eSYSCON_ClkoutSource_MainClock, 1U);
}

/* EOF. */

