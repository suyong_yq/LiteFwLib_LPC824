/* main.c */
#include "app_inc.h"

/*
* 注意：
* 对于没有特定模拟功能引脚，不需要配置SWM，上电复位之后默认的功能即是GPIO功能。
*/

int main(void)
{
    uint8_t ch;
    
    BSP_InitDebugUART(); /* 初始化调试串口 */
    
    printf("GPIO_BlinkyLED.\r\n");
    printf("Initializing...");

    /* 打开GPIO模块的端口访问时钟 */
    //Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO);
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_GPIO);
    
    /* 设定GPIO引脚方向为输出 */
    Chip_GPIO_SetPortDirOutput(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
        ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
    /* 设定GPIO引脚输出电平状态 */
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED0_PIN_ID, 1U);
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED1_PIN_ID, 0U);
    
    printf("Done.\r\n");

    while (1)
    {
        ch = getchar();
        putchar(ch);
        
        /* 翻转GPIO引脚输出电平，闪烁LED小灯 */
        Chip_GPIO_TogglePort(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
            ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
    }
}

/* EOF. */

