/* main.c */
#include "app_inc.h"

#define DEMO_PINT_IDX      0U
#define DEMO_GPIO_PIN_ID   BSP_GPIO_KEY_USER_PIN_ID

volatile uint32_t gPint0Counter = 0U; /* 对PINT0中断进行计数 */

void App_InitPINT0(void);

/*
* 注意：
* 对于没有特定模拟功能引脚，不需要配置SWM，上电复位之后默认的功能即是GPIO功能。
*/

int main(void)
{
    BSP_InitDebugUART(); /* 初始化调试串口 */
    
    printf("PINT_KeyInterrupt.\r\n");
    printf("Initializing...");

    /* 打开GPIO模块的端口访问时钟 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_GPIO);
    
    /* 设定GPIO引脚方向为输出 */
    Chip_GPIO_SetPortDirOutput(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
        ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
    /* 设定GPIO引脚输出电平状态 */
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED0_PIN_ID, 1U);
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED1_PIN_ID, 0U);
    
    /* 初始化PINT0中断监测模块 */
    App_InitPINT0();
    
    printf("Done.\r\n");

    while (1)
    {
        getchar(); /* 在串口终端界面触发一次主循环执行周期 */
        
        /* 翻转GPIO引脚输出电平，闪烁LED小灯 */
        Chip_GPIO_TogglePort(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
            ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
        
        printf("gPint0Counter = %d\r\n", gPint0Counter);
    }
}

/* 配置PINT0产生在USER_KEY对应的引脚上监测输入上升沿中断 */
void App_InitPINT0(void)
{
    /* 配置信号源 */
    Chip_SYSCON_SetPinInterrupt(DEMO_PINT_IDX, DEMO_GPIO_PIN_ID);
    
    /* 配置PINT属性 */
    Chip_PINT_SetPinInterruptMode(LPC_PINT, DEMO_PINT_IDX, ePINT_TriggerByEitherEdge); /* 上升沿触发 */

    /* 在NVIC中启用中断 */
    NVIC_EnableIRQ(PINT0_IRQn);
}

/* PINT0中断服务程序 */
void PINT0_IRQHandler(void)
{
    PINT_TriggerMode_T mode = Chip_PINT_GetPinInterruptStatus(LPC_PINT, DEMO_PINT_IDX);
    
    if ( ePINT_TriggerByRisingEdge == mode)
    {
        gPint0Counter++;
    }
    if ( ePINT_TriggerByFallingEdge == mode)
    {
        gPint0Counter++;
    }
    
    Chip_PINT_ClearPinInterruptStatus(LPC_PINT, DEMO_PINT_IDX); /* 清标志 */
}

/* EOF. */

