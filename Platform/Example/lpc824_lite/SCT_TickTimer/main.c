/* main.c */
#include "app_inc.h"

#define DEMO_SCT_EVENT_IDX_0 0U /* 使用事件(通道)的编号 */
#define DEMO_SCT_MATCH_REG_0 0U /* 选用SCT_MATCHn寄存器的编号 */
#define DEMO_SCT_STATE_IDX_0 0U /* 使用状态(混合事件)的编号 */

/*
* 注意：
* 对于没有特定模拟功能引脚，不需要配置SWM，上电复位之后默认的功能即是GPIO功能。
*/

int main(void)
{
    SCT_CounterConfig_T SctCounterConfigStructure;
    SCT_EventConditionConfig_T SctEventConditionConfigStructure;

    BSP_InitDebugUART(); /* 初始化调试串口 */

    printf("\r\n\r\nSCT_TickTimer.\r\n");
    printf("Initializing...\r\n");

    /* 打开模块的端口访问时钟 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_GPIO);
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SCT);
    /* 复位SCT模块硬件 */
    Chip_SYSCON_AssertPeriphReset(RESET_SCT);
    Chip_SYSCON_DeassertPeriphReset(RESET_SCT);

    /*
    * 配置GPIO控制LED灯
    */
    /* 设定GPIO引脚方向为输出 */
    Chip_GPIO_SetPortDirOutput(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
        ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
    /* 设定GPIO引脚输出电平状态 */
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED0_PIN_ID, 1U);
    Chip_GPIO_WritePin(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID, BSP_GPIO_LED1_PIN_ID, 0U);
    printf(" - GPIO-LED Ready.\r\n");

    /*
    * 配置SCT计数器
    */
    /* 配置计数器 */
    SctCounterConfigStructure.CounterMode = eSCT_CounterMode_32bitx1; /* 使用32bit计数器 */
    SctCounterConfigStructure.ClockSource = eSCT_ClockSource_SystemClock; /* 使用Bus Clock. */
    SctCounterConfigStructure.ClockEdge = eSCT_ClcokEdge_RisingEdgeOnInput0; /* Dummy setting. */
    SctCounterConfigStructure.EnableNoReloadCounterMaskValue = SCT_COUNTER_ID_Unify32bit_MASK;
    SctCounterConfigStructure.SyncInputMaskValue = SCT_COUNTER_ID_Unify32bit_MASK;
    SctCounterConfigStructure.EnableAutoLimitCounterMaskValue = SCT_COUNTER_ID_Unify32bit_MASK;
    Chip_SCT_ConfigCounter(LPC_SCT, &SctCounterConfigStructure);

    /* 配置Match Register/Match Reload Register. */
    Chip_SCT_SetEventMatchValue( LPC_SCT,
        SCT_COUNTER_ID_Unify32bit_MASK, /* counterIdMask. */
        DEMO_SCT_MATCH_REG_0,           /* matchRegIdx. */
        BSP_CLK_SYSTEM_CLK_HZ/5U        /* value. Load value in first time.*/
        );
    Chip_SCT_SetEventMatchReloadValue( LPC_SCT,
        SCT_COUNTER_ID_Unify32bit_MASK, /* counterIdMask. */
        DEMO_SCT_MATCH_REG_0,           /* matchRegIdx. */
        BSP_CLK_SYSTEM_CLK_HZ/5U        /* value. Reload value in future.*/
        );

    /* 配置Event */
    SctEventConditionConfigStructure.MatchRegIdx = DEMO_SCT_MATCH_REG_0; /* 使用MATCH0寄存器 */
    SctEventConditionConfigStructure.EventCondition = eSCT_EventCondition_MatchOnly; /* 仅在Match条件满足时触发 */
    SctEventConditionConfigStructure.EnableInStatesMaskValue = (1U << DEMO_SCT_STATE_IDX_0); /* 本事件仅可在State 0发生 */
    Chip_SCT_ConfigEventCondition(LPC_SCT, DEMO_SCT_EVENT_IDX_0, &SctEventConditionConfigStructure);
    /* 预先设定当Event 0发生时，回转(Limit)计数器的值 */
    Chip_SCT_PreSetEventLimitCounterCmd( LPC_SCT,
        SCT_COUNTER_ID_Unify32bit_MASK, /* counterIdMask. */
        (1U << DEMO_SCT_EVENT_IDX_0),   /* eventMask. */
        true                            /* enable. Enable the Limit action. */
    );

    /* 开事件中断 */
    Chip_SCT_EnableEventInterrupt(LPC_SCT, 1U << DEMO_SCT_EVENT_IDX_0, true);
    NVIC_EnableIRQ(SCT_IRQn);

    printf(" - SCT-Ticker Ready.\r\n");
    printf(" - All Done.\r\n");
    printf("Input any key to start the counter. ");
    getchar();
    printf("Go.\r\n");

    /* 启动计数器 */
    Chip_SCT_ControlCounter( LPC_SCT,
        SCT_COUNTER_ID_Unify32bit_MASK, /* counterIdMask. */
        eSCT_CounterCtrlCmd_HALT,       /* cmd. HALT命令 */
        0U                              /* param. 清HALT位以启动计数器 */
    );
    while (1)
    {
        __WFI();
    }
}

/*
* SCT中断服务程序
*/
void SCT_IRQHandler(void)
{
    /* 获取标志位 */
    uint32_t flags = Chip_SCT_GetEventFlag(LPC_SCT);

    if ( (1U << DEMO_SCT_EVENT_IDX_0) & flags )
    {
        /* 翻转GPIO引脚输出电平，闪烁LED小灯 */
        Chip_GPIO_TogglePort(BSP_GPIO_LEDx_BASEADDR, BSP_GPIO_LEDx_PORT_ID,
            ((1U<<BSP_GPIO_LED0_PIN_ID) | (1U<<BSP_GPIO_LED1_PIN_ID)) );
    }

    /* 清标志位 */
    Chip_SCT_ClearEventFlag(LPC_SCT, flags);
}

/* EOF. */

