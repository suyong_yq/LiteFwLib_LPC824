/* app_inc.h */

#ifndef __APP_INC_H__
#define __APP_INC_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "bsp_config.h"

#include "chip_syscon_8xx.h"
#include "chip_swm_8xx.h"
#include "chip_gpio_8xx.h"
#include "chip_spi_8xx.h"

void App_InitHardware(void)

#endif /* __APP_INC_H__ */

