/* hardware_init.c */
#include "app_inc.h"


/*
* SPIx_CS  - PIO0_15.
* SPIx_CLK - PIO0_24.
* SPIx_RX  - PIO0_25.
* SPIx_TX  - PIO0-26.
*/
void App_InitHardware(void)
{
    /* 配置电源 */

    /* 配合时钟 */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SPI0); /* 启用寄存器访问接口 */

    /* 复位模块 */
    Chip_SYSCON_PeriphReset(eSYSCON_PeriphReset_SPI0);

#if 0
    /* 配置端口引脚， IOCON */
    LPC_IOCON->PIO0_15 = ;
    LPC_IOCON->PIO0_24 = ;
    LPC_IOCON->PIO0_25 = ;
    LPC_IOCON->PIO0_26 = ;
#endif

    /* 配置引脚复用功能, SWM */
    Chip_SYSCON_EnablePeriphClock(SYSCON_AHBPeriphClk_SWM);
    LPC_SWM->PINASSIGN[3] = (LPC_SWM->PINASSIGN[3] & ~SWM_PINASSIGN3_SPI0_SCK_IO_MASK)
                          | SWM_PINASSIGN3_SPI0_SCK_IO(24U); /* SPIx_CLK - PIO0_24. */
    LPC_SWM->PINASSIGN[4] = (LPC_SWM->PINASSIGN[4] & ~(  SWM_PINASSIGN4_SPI0_MOSI_IO_MASK
                                                       | SWM_PINASSIGN4_SPI0_MISO_IO_MASK
                                                       | SWM_PINASSIGN4_SPI0_SSEL0_IO_MASK
                                                    )
                          | SWM_PINASSIGN4_SPI0_MOSI_IO(26U) /* SPIx_TX  - PIO0-26. */
                          | SWM_PINASSIGN4_SPI0_MISO_IO(25U) /* SPIx_RX  - PIO0_25. */
                          | SWM_PINASSIGN4_SPI0_SSEL0_IO(15U) /* SPIx_CS  - PIO0_15. */
                           );

    Chip_SYSCON_DisablePeriphClock(SYSCON_AHBPeriphClk_SWM);

}

/* EOF. */

