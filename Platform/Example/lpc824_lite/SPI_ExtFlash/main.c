/* main.c */
#include "app_inc.h"

/*
* 注意：
* 对于没有特定模拟功能引脚，不需要配置SWM，上电复位之后默认的功能即是GPIO功能。
*/

int main(void)
{
    uint8_t ch;
    
    BSP_InitDebugUART(); /* 初始化调试串口 */
    App_InitHardware();
    
    printf("SPI_ExtFlash.\r\n");
    printf("Initializing...");

    /* 初始化SPI0 */
    


    printf("Done.\r\n");

    while (1)
    {
        ch = getchar();
        putchar(ch);
        
    }
}

void ExtFlash_SPIx_Configuration(void)
{
    SPI_MasterConfig_T mSpiMasterConfigStruct;
    Chip_SPI_InitMaster(BSP_SPI_EXT_FLASH_BASEADDR, &mSpiMasterConfigStruct);
}

/* EOF. */

