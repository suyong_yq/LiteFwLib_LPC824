/* main.c */
#include "app_inc.h"

int main(void)
{
	uint8_t ch;

    BSP_InitDebugUART(); /* 初始化调试串口 */

    printf("Hello, World\r\n");
    
    while (1)
    {
        ch = getchar();
        putchar(ch);
    }
}
/* EOF. */

